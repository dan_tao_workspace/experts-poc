"""Sync the contents of the specified file to default reviewers."""

import os
import re
import sys

import requests


def require_env_var(name):
    """Get the value of the specified environment variable.

    Exit the process if the variable isn't defined.
    """
    value = os.getenv(name)
    if value is None:
        print('The {} environment variable is required.'.format(name))
        sys.exit(1)
    return value

FILE_NAME = require_env_var('FILE_NAME')
BB_USERNAME = require_env_var('BB_USERNAME')
BB_PASSWORD = require_env_var('BB_PASSWORD')
BB_REPOSITORY = require_env_var('BITBUCKET_REPO_FULL_NAME')


class Client:
    """Super simple API client for adding default reviewers."""

    def __init__(self, username, password):
        """Initialize API client with username and app password."""
        self.base_url = 'https://api.bitbucket.org/2.0/repositories'
        self.session = requests.Session()
        self.session.auth = (username, password)

    def add_default_reviewer(self, username):
        """Add the specified user as a default reviewer using the API."""
        resp = self.session.put('{}/{}/default-reviewers/{}'.format(
            self.base_url, BB_REPOSITORY, username))
        if not resp.ok:
            print('Unable to add "{}" as a default reviewer:'.format(username))
            resp.raise_for_status()

if __name__ == '__main__':
    client = Client(BB_USERNAME, BB_PASSWORD)

    with open(FILE_NAME) as f:
        for username in f.readlines():
            username = username.strip()
            if re.match(r'^[\w-]+$', username):
                client.add_default_reviewer(username)
                print('Added "{}" as default reviewer.'.format(username))
