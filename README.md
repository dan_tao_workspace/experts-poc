# Experts

This is a proof of concept for managing a list of "experts" via source code. (It doesn't have to be experts. It can really be any list of users.)

## The idea

1. Define a list of users in a file
2. Set the repository's default reviewers to the same list of user
3. Enable branch permissions to require all changes to go through pull requests, and merge checks to enforce that all pull requests are approved by >= N default reviewers
4. Configure Pipelines to sync the contents of the file defined in step 1 to the repository's default reviewers
